<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "api" middleware group. Make something great!
|
*/


Route::post('auth/signin', [\App\Infrastructure\Http\Controllers\AuthController::class, 'signIn']);
Route::post('auth/signup', [\App\Infrastructure\Http\Controllers\AuthController::class, 'signUp']);

Route::middleware('auth:sanctum')->group(function () {
    Route::get('auth/me',  [\App\Infrastructure\Http\Controllers\AuthController::class, 'me']);
    Route::apiResource('items', \App\Infrastructure\Http\Controllers\TodoItemController::class, ['only' => ['index', 'store', 'update', 'destroy']]);
    Route::post('items/reorder', [\App\Infrastructure\Http\Controllers\TodoItemController::class, 'reorder']);
    Route::post('items/{id}/markAsCompleted', [\App\Infrastructure\Http\Controllers\TodoItemController::class, 'markAsCompleted']);
    Route::post('items/{id}/markAsUncompleted', [\App\Infrastructure\Http\Controllers\TodoItemController::class, 'markAsIncompleted']);
});
