<?php

namespace App\Domain\Todo\Entities;

use Illuminate\Database\Eloquent\Model;

/**
 * TodoItem entity
 * @property int $id
 * @property string $title
 * @property string $description
 * @property int $order
 * @property \DateTime $created_at
 * @property \DateTime $updated_at
 */
class TodoItem extends Model
{
    /**
     * Table name
     */
    protected $table = 'todo_items';
    
    protected $casts = ['is_completed' => 'bool'];
    
    protected $fillable = ['title', 'description', 'is_completed', 'order', 'user_id'];
}
