<?php

namespace App\Domain\Todo\Repositories;

interface TodoItemRepository
{
    public function findByUserId($id);
    public function create($title, $description, $userId);
    public function update($title, $description, $id, $userId);
    public function destroy($id, $userId);
    public function markAsCompleted($id, $userId);
    public function markAsIncompleted($id, $userId);
    public function reorder($order, $id, $userId);
}
