<?php

namespace App\Domain\User\Repositories;

interface UserRepository
{
    public function findByEmail($email);
    public function create($email, $password);
}
