<?php

namespace App\Domain\User\Entities;

use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;
use Laravel\Sanctum\HasApiTokens;

/**
 * User entity
 * @property int $id
 * @property string $email
 * @property \DateTime $created_at
 * @property \DateTime $updated_at
 */
class User extends Model
{
    use Authenticatable,
        HasApiTokens;
    
    /**
     * Table name
     */
    protected $table = 'users';
    
    protected $fillable = ['email', 'password'];
}
