<?php

namespace App\Application\Services;

interface PasswordHash {
    public function hash(string $string): string;
    public function compare(string $string, string $hash): bool;
}

