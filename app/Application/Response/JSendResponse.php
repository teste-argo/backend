<?php

namespace App\Application\Response;

use Illuminate\Http\Response;

class JSendResponse
{
    public function __construct(readonly string $status, readonly mixed $content, readonly int $httpCode = Response::HTTP_OK)
    {
    }
    
    public function toJson()
    {
        $responseData = ['status' => $this->status, 'data' => $this->content];
        
        if (isset($this->content['message'])) {
            $responseData['message'] = $this->content['message'];
            unset($responseData['data']['message']);
        }
        
        if (empty($responseData['data'])) {
            unset($responseData['data']);
        }
        
        return response()->json($responseData, $this->httpCode) ?? '';
    }
}
