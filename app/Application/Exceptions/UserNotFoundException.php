<?php

namespace App\Application\Exceptions;

use App\Application\Response\JSendResponse;

class UserNotFoundException extends \Exception
{
    public function render()
    {
        return (new JSendResponse('fail', ['message' => 'Usuário ou senha inválidos.'], 401))->toJson();
    }
}
