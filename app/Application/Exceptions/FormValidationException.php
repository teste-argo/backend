<?php

namespace App\Application\Exceptions;

use App\Application\Response\JSendResponse;
use Illuminate\Http\Response;
use Illuminate\Validation\ValidationException;

class FormValidationException extends ValidationException
{
    public function render()
    {
        $response = new JSendResponse('fail', [
            'message'   => 'Failed validation.',
            'errors'    => $this->validator->errors(),
            'all_errors'=> $this->validator->messages()->all(),
        ], Response::HTTP_UNPROCESSABLE_ENTITY);
        
        return $response->toJson();
    }
}
