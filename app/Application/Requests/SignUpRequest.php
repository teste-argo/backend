<?php

namespace App\Application\Requests;

use App\Infrastructure\Requests\FormRequestAPI;

class SignUpRequest extends FormRequestAPI
{
    public function rules()
    {
        return [
            'email' => 'required|email|unique:users,email',
            'password' => 'required'
        ];
    }
}
