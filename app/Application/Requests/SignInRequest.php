<?php

namespace App\Application\Requests;

use App\Infrastructure\Requests\FormRequestAPI;

class SignInRequest extends FormRequestAPI
{
    public function rules()
    {
        return [
            'email' => 'required|email',
            'password' => 'required'
        ];
    }
}
