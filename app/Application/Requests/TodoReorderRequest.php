<?php

namespace App\Application\Requests;

use App\Infrastructure\Requests\FormRequestAPI;

class TodoReorderRequest extends FormRequestAPI
{
    public function rules()
    {
        return [
            'items' => 'required|array',
            'items.*' => 'required|integer'
        ];
    }
}
