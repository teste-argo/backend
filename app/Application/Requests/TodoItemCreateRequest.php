<?php

namespace App\Application\Requests;

use App\Infrastructure\Requests\FormRequestAPI;

class TodoItemCreateRequest extends FormRequestAPI
{
    public function rules()
    {
        return [
            'title' => 'required',
            'description' => 'required'
        ];
    }
}
