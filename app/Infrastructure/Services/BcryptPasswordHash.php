<?php

namespace App\Infrastructure\Services;

use App\Application\Services\PasswordHash;

class BcryptPasswordHash implements PasswordHash {
    public function hash(string $string): string {
        return password_hash($string, PASSWORD_BCRYPT);
    }
    
    public function compare(string $string, string $hash): bool {
        return password_verify($string, $hash);
    }
}

