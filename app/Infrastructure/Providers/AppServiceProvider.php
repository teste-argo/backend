<?php

namespace App\Infrastructure\Providers;

use App\Application\Services\PasswordHash;
use App\Domain\Todo\Repositories\TodoItemRepository;
use App\Domain\User\Repositories\UserRepository;
use App\Infrastructure\Eloquent\EloquentTodoItemRepository;
use App\Infrastructure\Eloquent\EloquentUserRepository;
use App\Infrastructure\Services\BcryptPasswordHash;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     */
    public function register(): void
    {
        $this->app->bind(UserRepository::class, function ($app) {
            return new EloquentUserRepository();
        });
        $this->app->bind(TodoItemRepository::class, function ($app) {
            return new EloquentTodoItemRepository();
        });
        $this->app->bind(PasswordHash::class, function ($app) {
            return new BcryptPasswordHash();
        });
    }

    /**
     * Bootstrap any application services.
     */
    public function boot(): void
    {
        //
    }
}
