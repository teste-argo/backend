<?php

namespace App\Infrastructure\Requests;

use App\Application\Exceptions\FormValidationException;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Foundation\Http\FormRequest;

abstract class FormRequestAPI extends FormRequest
{
    public function authorize(): bool
    {
        return true;
    }
    
    protected function failedValidation(Validator $validator)
    {
        throw new FormValidationException($validator);
    }
}
