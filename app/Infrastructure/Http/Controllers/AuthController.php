<?php

namespace App\Infrastructure\Http\Controllers;

use App\Application\Exceptions\UserNotFoundException;
use App\Application\Requests\SignInRequest;
use App\Application\Requests\SignUpRequest;
use App\Application\Resources\UserResource;
use App\Application\Response\JSendResponse;
use App\Application\Services\PasswordHash;
use App\Domain\User\Repositories\UserRepository;
use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function signIn(SignInRequest $request, UserRepository $repository, PasswordHash $hasher)
    {
        $user = $repository->findByEmail($request->email);
        
        if (!$user || !$hasher->compare($request->password, $user->getAuthPassword())) {
            throw new UserNotFoundException();
        }
        
        $token = $user->createToken('DEFAULT_LOGIN');
        $resource = new UserResource($user);
        $response = new JSendResponse('success', ['user' => $resource, 'token' => $token->plainTextToken]);
        
        return $response->toJson();
    }
    
    public function signUp(SignUpRequest $request, UserRepository $repository, PasswordHash $hasher)
    {
        $user = $repository->create($request->email, $hasher->hash($request->password));
        $token = $user->createToken('DEFAULT_LOGIN');
        $resource = new UserResource($user);
        $response = new JSendResponse('success', ['user' => $resource, 'token' => $token->plainTextToken]);
        
        return $response->toJson();
    }
    
    public function me(Request $request)
    {
        $user = $request->user();
        $response = new JSendResponse('success', ['user' => $user]);
        
        return $response->toJson();
    }
}
