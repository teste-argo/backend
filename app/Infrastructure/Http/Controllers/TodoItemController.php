<?php

namespace App\Infrastructure\Http\Controllers;

use App\Application\Requests\TodoItemCreateRequest;
use App\Application\Requests\TodoReorderRequest;
use App\Application\Response\JSendResponse;
use App\Domain\Todo\Entities\TodoItem;
use App\Domain\Todo\Repositories\TodoItemRepository;

class TodoItemController extends Controller
{
    public function index(TodoItemRepository $repository)
    {
        $items = $repository->findByUserId(auth()->user()->id);
        $response = new JSendResponse('success', ['items' => $items]);
        
        return $response->toJson();
    }
    
    public function store(TodoItemCreateRequest $request, TodoItemRepository $repository)
    {
        $item = $repository->create($request->title, $request->description, auth()->user()->id);
        $response = new JSendResponse('success', ['item' => $item]);
        
        return $response->toJson();
    }
    
    public function update($id, TodoItemCreateRequest $request, TodoItemRepository $repository)
    {
        $repository->update($request->title, $request->description, $id, auth()->user()->id);
        $response = new JSendResponse('success', []);
        
        return $response->toJson();
    }
    
    public function destroy($id, TodoItemRepository $repository)
    {
        $repository->destroy($id, auth()->user()->id);
        $response = new JSendResponse('success', []);
        
        return $response->toJson();
    }
    
    public function reorder(TodoReorderRequest $request, TodoItemRepository $repository)
    {
        foreach ($request->items as $key => $item) {
            TodoItem::query()
                ->where('id', $item)
                ->update(['order' => $key]);
        }
        
        $response = new JSendResponse('success', []);
        
        return $response->toJson();
    }
    
    public function markAsCompleted($id, TodoItemRepository $repository)
    {
        $repository->markAsCompleted($id, auth()->user()->id);
        $response = new JSendResponse('success', []);
        
        return $response->toJson();
    }
    
    public function markAsIncompleted($id, TodoItemRepository $repository)
    {
        $repository->markAsIncompleted($id, auth()->user()->id);
        $response = new JSendResponse('success', []);
        
        return $response->toJson();
    }
}
