<?php

namespace App\Infrastructure\Eloquent;

use App\Domain\User\Entities\User;
use App\Domain\User\Repositories\UserRepository;

class EloquentUserRepository implements UserRepository
{
    public function findByEmail($email)
    {
        return User::query()->where('email', $email)->first();
    }
    
    public function create($email, $password)
    {
        return User::query()->create([
            'email' => $email,
            'password' => $password,
        ]);
    }
}
