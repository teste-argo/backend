<?php

namespace App\Infrastructure\Eloquent;

use App\Domain\Todo\Entities\TodoItem;
use App\Domain\Todo\Repositories\TodoItemRepository;

class EloquentTodoItemRepository implements TodoItemRepository
{
    public function create($title, $description, $userId)
    {
        $lastOrder = TodoItem::query()->where('user_id', $userId)->selectRaw('MAX(`order`) as lastOrder')->first();
        return TodoItem::query()->create([
            'title' => $title,
            'description' => $description,
            'user_id' => $userId,
            'order' => $lastOrder->lastOrder + 1,
        ]);
    }
    
    public function update($title, $description, $id, $userId)
    {
        $data = [];
        
        if ($title) {
            $data['title'] = $title;
        }
        
        if ($description) {
            $data['description'] = $description;
        }
        
        return TodoItem::query()->where('id', $id)
            ->where('user_id', $userId)
            ->update($data);
    }
    
    public function markAsCompleted($id, $userId)
    {
        return TodoItem::query()->where('id', $id)
            ->where('user_id', $userId)
            ->update(['is_completed' => true]);
    }
    
    public function markAsIncompleted($id, $userId)
    {
        return TodoItem::query()->where('id', $id)
            ->where('user_id', $userId)
            ->update(['is_completed' => false]);
    }
    
    public function findByUserId($id)
    {
        return TodoItem::query()->where('user_id', $id)
            ->orderBy('order')
            ->get();
    }
    
    public function destroy($id, $userId)
    {
        return TodoItem::query()->where('id', $id)
            ->where('user_id', $userId)
            ->delete();
    }
    
    public function reorder($order, $id, $userId)
    {
        return TodoItem::query()->where('user_id', $userId)
            ->where('id', $id)
            ->update(['order' => $order]);
    }
}
